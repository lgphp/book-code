package com.mygame.db.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import com.mygame.db.entity.UserAccount;

public interface UserAccountRepository extends MongoRepository<UserAccount, String> {


}
