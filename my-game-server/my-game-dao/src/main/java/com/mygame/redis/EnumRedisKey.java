package com.mygame.redis;

import java.time.Duration;
import org.springframework.util.StringUtils;

public enum EnumRedisKey {
    USER_ID_INCR(null), // UserId 自增key

    USER_ACCOUNT(Duration.ofDays(7)), // 用户信息

    PLAYER_ID_INCR(null),

    PLAYER_NICKNAME(null),
    PLAYER_INFO(Duration.ofDays(7)),
    ARENA(Duration.ofDays(7)),
    
    ;
    private Duration timeout;// 此key的value的expire时间,如果为null，表示value永远不过期

    private EnumRedisKey(Duration timeout) {
        this.timeout = timeout;
    }

    public String getKey(String id) {
        if (StringUtils.isEmpty(id)) {
            throw new IllegalArgumentException("参数不能为空");
        }
        return this.name() + "_" + id;
    }

    public Duration getTimeout() {
        return timeout;
    }

    public String getKey() {
        return this.name();
    }
    
    public static void main(String[] args) {
        
    }

}
