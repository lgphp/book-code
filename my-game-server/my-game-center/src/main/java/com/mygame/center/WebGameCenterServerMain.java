package com.mygame.center;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@SpringBootApplication(scanBasePackages= {"com.mygame"})
@EnableMongoRepositories(basePackages= {"com.mygame"})
public class WebGameCenterServerMain {
    
    public static void main(String[] args) {
        SpringApplication.run(WebGameCenterServerMain.class, args);
    }
}




//不想在服务启动的时候初始化数据库的连接信息
//@SpringBootApplication(scanBasePackages= {"com.mygame"},exclude= {MongoAutoConfiguration.class,MongoDataAutoConfiguration.class})