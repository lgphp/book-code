package com.mygame.gateway.message.channel;

import io.netty.util.concurrent.GenericFutureListener;

public interface GameChannelFutureListener extends GenericFutureListener<GameChannelFuture> {

}
