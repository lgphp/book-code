package com.mygame.gateway.server.handler.codec;

import com.mygame.common.utils.AESUtils;
import com.mygame.common.utils.CompressUtil;
import com.mygame.game.common.GameMessageHeader;
import com.mygame.game.common.GameMessagePackage;
import com.mygame.gateway.server.GatewayServerConfig;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;

/**
 * 
 * @ClassName: EncodeHandler
 * @Description: 编码服务器向客户端发送的数据
 * @author: wgs
 * @date: 2019年4月7日 下午3:04:31
 */
public class EncodeHandler extends MessageToByteEncoder<GameMessagePackage> {
    private static final int GAME_MESSAGE_HEADER_LEN = 29;
    private GatewayServerConfig serverConfig;

    public EncodeHandler(GatewayServerConfig serverConfig) {
        this.serverConfig = serverConfig;// 注入服务端配置
    }

    private String aesSecret;// 对称加密密钥

    public void setAesSecret(String aesSecret) {
        this.aesSecret = aesSecret;
    }

    @Override
    protected void encode(ChannelHandlerContext ctx, GameMessagePackage msg, ByteBuf out) throws Exception {
        int messageSize = GAME_MESSAGE_HEADER_LEN;
        byte[] body = msg.getBody();
        int compress = 0;
        if (body != null) {// 达到压缩条件，进行压缩

            if (body.length >= serverConfig.getCompressMessageSize()) {
                body = CompressUtil.compress(body);
                compress = 1;
            }
            if (this.aesSecret != null && msg.getHeader().getMessageId() != 1) {
                 body = AESUtils.encode(aesSecret, body);
            }
            messageSize += body.length;
        }
        out.writeInt(messageSize);
        GameMessageHeader header = msg.getHeader();
        out.writeInt(header.getClientSeqId());
        out.writeInt(header.getMessageId());
        out.writeLong(header.getServerSendTime());
        out.writeInt(header.getVersion());
        out.writeByte(compress);
        out.writeInt(header.getErrorCode());
        if (body != null) {
            out.writeBytes(body);
        }
    }
}
