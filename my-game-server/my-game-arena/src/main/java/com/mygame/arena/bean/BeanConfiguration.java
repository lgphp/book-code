package com.mygame.arena.bean;

import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import com.mygame.common.concurrent.GameEventExecutorGroup;
import com.mygame.dao.ArenaDao;
import com.mygame.dao.AsyncArenaDao;
import com.mygame.gateway.message.context.ServerConfig;

@Configuration
public class BeanConfiguration {

    @Autowired
    private ServerConfig serverConfig;//注入配置信息
    
    private GameEventExecutorGroup dbExecutorGroup;
    @Autowired
    private ArenaDao arenaDao; //注入数据库操作类
    
    @PostConstruct
    public void init() {
        dbExecutorGroup = new GameEventExecutorGroup(serverConfig.getDbThreads());//初始化db操作的线程池组
    }
    
    @Bean
    public AsyncArenaDao asyncPlayerDao() {//配置AsyncPlayerDao的Bean
        return new AsyncArenaDao(dbExecutorGroup, arenaDao);
    }
}
