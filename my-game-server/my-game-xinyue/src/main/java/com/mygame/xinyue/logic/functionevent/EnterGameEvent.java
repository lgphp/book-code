package com.mygame.xinyue.logic.functionevent;

import org.springframework.context.ApplicationEvent;
import com.mygame.db.entity.manager.PlayerManager;

public class EnterGameEvent extends ApplicationEvent{

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private PlayerManager playerManager;
    public EnterGameEvent(Object source,PlayerManager playerManager) {
        super(source);
        this.playerManager= playerManager;
    }
    public PlayerManager getPlayerManager() {
        return playerManager;
    }
    
    
    
}
