package com.mygame.xinyue;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import com.mygame.dao.AsyncPlayerDao;
import com.mygame.game.messagedispatcher.DispatchGameMessageService;
import com.mygame.gateway.message.context.DispatchUserEventService;
import com.mygame.gateway.message.context.GatewayMessageConsumerService;
import com.mygame.gateway.message.context.ServerConfig;
import com.mygame.gateway.message.handler.GameChannelIdleStateHandler;
import com.mygame.xinyue.common.GameBusinessMessageDispatchHandler;

@SpringBootApplication(scanBasePackages = {"com.mygame"})
@EnableMongoRepositories(basePackages = {"com.mygame"}) // 负责连接数据库
public class XinyueGameServerMain {

    public static void main(String[] args) {
        ApplicationContext context = SpringApplication.run(XinyueGameServerMain.class, args);//初始化spring boot环境
        ServerConfig serverConfig = context.getBean(ServerConfig.class);//获取配置的实例
        DispatchGameMessageService.scanGameMessages(context, serverConfig.getServiceId(), "com.mygame");// 扫描此服务可以处理的消息
        GatewayMessageConsumerService gatewayMessageConsumerService = context.getBean(GatewayMessageConsumerService.class);//获取网关消息监听实例
        //PlayerDao playerDao = context.getBean(PlayerDao.class);//获取Player数据操作类实例
        AsyncPlayerDao playerDao = context.getBean(AsyncPlayerDao.class);
        DispatchGameMessageService dispatchGameMessageService= context.getBean(DispatchGameMessageService.class);
        DispatchUserEventService dispatchUserEventService = context.getBean(DispatchUserEventService.class);
         
        gatewayMessageConsumerService.start((gameChannel) -> {//启动网关消息监听，并初始化GameChannelHandler
            // 初始化channel
            gameChannel.getChannelPiple().addLast(new GameChannelIdleStateHandler(300, 300, 300));
            gameChannel.getChannelPiple().addLast(new GameBusinessMessageDispatchHandler(context,serverConfig,dispatchGameMessageService,dispatchUserEventService, playerDao));
        },serverConfig.getServerId());
    }

}
