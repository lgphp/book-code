package com.mygame.xinyue.service;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.MockitoTestExecutionListener;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.Test;
import com.mygame.db.entity.Hero;
import com.mygame.db.entity.Prop;
import com.mygame.db.entity.Weapon;
import com.mygame.db.entity.manager.HeroManager;
import com.mygame.db.entity.manager.InventoryManager;
import com.mygame.db.entity.manager.PlayerManager;
import com.mygame.xinyue.common.DataConfigService;
import com.mygame.xinyue.dataconfig.EquipWeaponDataConfig;
@SpringBootTest(classes = {HeroWeaponService.class,DataConfigService.class})
@TestExecutionListeners(listeners = MockitoTestExecutionListener.class)
public class HeroWeaponServiceTest extends AbstractTestNGSpringContextTests {
    @SpyBean
    private HeroWeaponService heroWeaponService;
    @MockBean
    private DataConfigService dataConfigService;
    
    @Test
    public void addHeroWeapon() {
        //使用Mockito创建类的实例，这样创建的实例可以指定方法的返回值，用于手动根据测试需要构造数据
        PlayerManager playerManager = Mockito.mock(PlayerManager.class);
        HeroManager heroManager = Mockito.mock(HeroManager.class);
        //返回指定的heroManager
        Mockito.when(playerManager.getHeroManager()).thenReturn(heroManager);
        Hero hero = new Hero();
        String heroId = "101";
        //返回指定的hero
        Mockito.when(heroManager.getHero(heroId)).thenReturn(hero);
        InventoryManager inventoryManager = Mockito.mock(InventoryManager.class);
        //返回指定的背包管理类
        Mockito.when(playerManager.getInventoryManager()).thenReturn(inventoryManager);
        Weapon weapon = new Weapon();
        String weaponId = "w101";
        //返回指定的weapon实例
        Mockito.when(inventoryManager.getWeapon(weaponId)).thenReturn(weapon);
        EquipWeaponDataConfig equipWeaponDataConfig = new EquipWeaponDataConfig();
        equipWeaponDataConfig.setLevel(10);
        equipWeaponDataConfig.setCostCount(10);
        //返回指定的数据配置类
        Mockito.when(dataConfigService.getDataConfig(weaponId, EquipWeaponDataConfig.class)).thenReturn(equipWeaponDataConfig);
        hero.setLevel(11);
        equipWeaponDataConfig.setCostId("201");
        Prop prop = new Prop();
        prop.setCount(20);
        //返回指定的道具
        Mockito.when(inventoryManager.getProp(equipWeaponDataConfig.getCostId())).thenReturn(prop);
        //调用要测试的方法
        heroWeaponService.addHeroWeapon(playerManager, heroId, weaponId);
        //验证结果的正确性
        assertEquals(hero.getWeaponId(), weaponId);
        assertFalse(weapon.isEnable());
    }
}
