package com.mygame.xinyue.service;

import static org.testng.Assert.assertEquals;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.reflect.Whitebox;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.MockitoTestExecutionListener;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.IObjectFactory;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.ObjectFactory;
import org.testng.annotations.Test;

@SpringBootTest(classes = {TestMockBean.class,TestSpyBean.class})
@TestExecutionListeners(listeners = MockitoTestExecutionListener.class)//必须有这个注解，要不然@SpyBean和@MockBean标记的类会为null
@PowerMockIgnore({"org.springframework.*","javax.*","org.mockito.*"})
@PrepareForTest(TestSpyBean.class)
public class SpringBeanTest extends AbstractTestNGSpringContextTests{//必须继承这个类
    @SpyBean
    private TestSpyBean testSpyBean;//注入要测试的类,使用SpyBean标记
    @MockBean
    private TestMockBean testMockBean; //注入要测试的类，使用MockBean标记
    @BeforeMethod
    public void setUp() {
    	Mockito.reset(testSpyBean);
    	Mockito.reset(testMockBean);
    }
    @ObjectFactory
    public IObjectFactory getObjectFactory() {
        return new org.powermock.modules.testng.PowerMockObjectFactory();
    }
    @Test
    public void testGetValue() {
        //不指定返回直接，直接调用
        int value = testSpyBean.getValue();
        assertEquals(value, 3);
        int value2 = testMockBean.getValue();
        assertEquals(value2, 0);//这里会失败，因为没有指定返回值，value2的值是默认值0
    }
    @Test
    public void testGetSpecialValue() {
        //都指定返回值
        //Mockito.doReturn(30).when(testSpyBean).getValue();
       // Mockito.when(testSpyBean.getValue()).thenReturn(30);
       //PowerMockito.when(testMockBean.getValue()).thenReturn(30);
        Mockito.doNothing().when(testMockBean).saveToRedis("a");
        testMockBean.saveToRedis("a");
        PowerMockito.doReturn(30).when(testSpyBean).getValue();
        Mockito.when(testMockBean.getValue()).thenReturn(100);
        int value = testSpyBean.getValue();
        assertEquals(value, 30);
        int value2 = testMockBean.getValue();
        assertEquals(value2, 100);
        
        
        String str = "abc";
        Mockito.doNothing().when(testMockBean).saveToRedis(str);
        testSpyBean.saveData(str);
        Mockito.verify(testSpyBean).saveData(str);//默认验证调用了一次
        Mockito.verify(testMockBean,Mockito.times(1)).saveToRedis(str);//还可以指定验证调用了多少次
        
        
        //mock静态方法
        PowerMockito.mockStatic(TestSpyBean.class);
        PowerMockito.when(TestSpyBean.queryValue()).thenReturn(200);
        value = TestSpyBean.queryValue();
        assertEquals(value, 200);
        PowerMockito.verifyStatic(TestSpyBean.class);//验证静态方法是否执行到
        TestSpyBean.queryValue();//要验证的静态方法
    }
    @DataProvider 
    public Object[][] data1(){//提供数据的方法
        Object[][] data = {
                {1,100},
                {2,200},
                {3,300},
                {4,500}
        };
        return data;
    }
    @Test(dataProvider = "data1")//指定提供数据的方法名
    public void testDataProdiver(int type,int result) {
        int value = testSpyBean.getValue(type);
        assertEquals(value, result);
    }
    
    @Test
    public void getName() throws Exception {//测试私有方法时，会有检查异常
    	String value = "adssfd";
    	testSpyBean = PowerMockito.spy(applicationContext.getBean(TestSpyBean.class));
    	PowerMockito.doReturn(value).when(testSpyBean,"getName",Mockito.anyInt());
    	String name = Whitebox.invokeMethod(testSpyBean, "getName",1);
    	assertEquals(name, value);
    	PowerMockito.verifyPrivate(testSpyBean).invoke("getName",1);//验证执行了一次
    	PowerMockito.verifyPrivate(testSpyBean,Mockito.times(1)).invoke("getName",1);//在times中指定要验证的执行次数
    }
    @Test 
    public void testDoAnswer() throws Exception {
    	testSpyBean = PowerMockito.spy(applicationContext.getBean(TestSpyBean.class));
    	PowerMockito.doAnswer(answer->{
    		int value = answer.getArgument(0);
    		assertEquals(value, 12);
    		return null;
    	}).when(testSpyBean,"getName",Mockito.anyInt());
    	testSpyBean.calculate(3);
    }
    
}
