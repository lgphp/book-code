package com.mygame.client;

import static org.testng.Assert.assertEquals;
import org.springframework.boot.test.context.SpringBootTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import com.mygame.db.entity.Player;
import com.mygame.db.entity.manager.PlayerManager;
import com.mygame.game.message.xinyue.EnterGameMsgRequest;
import com.mygame.game.message.xinyue.EnterGameMsgResponse;
import com.mygame.test.AbstractXinyueIntegrationTest;
import com.mygame.xinyue.XinyueGameServerMain;
@SpringBootTest(classes=XinyueGameServerMain.class)
public class EnterGameTest2 extends AbstractXinyueIntegrationTest<PlayerManager>{
    private long playerId = 1;
    private Player player;
    private PlayerManager playerManager;
    @BeforeMethod 
    public void init() {
        player = new Player();
        player.setPlayerId(1);
        playerManager = new PlayerManager(player);
    }
    @Test(description = "正常进入游戏测试")
    public void enterGameOk() {
        EnterGameMsgRequest request = new EnterGameMsgRequest();
        this.sendGameMessage(playerId,playerManager, request,c->{
            EnterGameMsgResponse response = (EnterGameMsgResponse) c;
            assertEquals(response.getHeader().getPlayerId(),playerId);
            assertEquals(response.getBodyObj().getPlayerId(), playerId);
        });
    }
}
