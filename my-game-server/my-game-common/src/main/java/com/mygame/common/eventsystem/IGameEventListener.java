package com.mygame.common.eventsystem;

public interface IGameEventListener {

    public void update(Object origin,IGameEventMessage event);
}
