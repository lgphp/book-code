package com.mygame.im.handler;

import org.springframework.context.ApplicationContext;
import com.mygame.db.entity.manager.IMManager;
import com.mygame.gateway.message.channel.AbstractGameChannelHandlerContext;
import com.mygame.gateway.message.channel.GameChannelPromise;
import com.mygame.gateway.message.handler.AbstractGameMessageDispatchHandler;
import io.netty.util.concurrent.Future;
import io.netty.util.concurrent.Promise;

public class GameIMHandler extends AbstractGameMessageDispatchHandler<IMManager>{
    private IMManager imManager;
    public GameIMHandler(ApplicationContext applicationContext) {
        super(applicationContext);
    }

    @Override
    protected IMManager getDataManager() {
        return imManager;
    }

    @Override
    protected Future<Boolean> updateToRedis(Promise<Boolean> promise) {
        promise.setSuccess(true);
        return promise;
    }

    @Override
    protected Future<Boolean> updateToDB(Promise<Boolean> promise) {
        promise.setSuccess(true);
        return promise;
    }

    @Override
    protected void initData(AbstractGameChannelHandlerContext ctx, long playerId, GameChannelPromise promise) {
            imManager = new IMManager();
            promise.setSuccess();
    }

}
